# Projeto Orientado a Computação - Ygor de Paula

Apresentado como parte da disciplina de Projeto Orientado a Conclusão (POC) no curso
de Ciência da Computação na Universidade Federal de São João del Rei (UFSJ) este
trabalho faz uso da definição formal de uma linguagem de programação em uma forma
parecida com a BNF para a configuração de um interpretador genérico para que este
aceite, de forma simples, a criação de linguagens de programação.

## Texto Apresentado

Os arquivos fonte em LaTeX do texto apresentado estão disponiveis na pasta *texto_POC*,
à partir destes é possivel gerar um arquivo pdf utilizando o **Makefile** disponibilizado
ou com os comandos:

```
pdflatex ufsj-abntex2.tex
bibtex ufsj-abntex2
pdflatex ufsj-abntex2.tex
pdflatex ufsj-abntex2.tex
```

## Apresentações da matéria

Na pasta *apresentacoes* é possivel encontrar as apresentações realizadas para os
professores durante a disciplina no formato **PDF**.

## Codigo-Fonte

O codigo fonte foi escrito em **JavaScript** e está disponível na pasta *source*.

### Pre-requisitos

* [Node.js](https://nodejs.org/en/) - Ambiente de execução JavaScript

### Executando

Para executar o interpretador basta executar o comando:
```
node analyzer.js <arquivo_de_gramática> <arquivo_fonte>
```

Onde *arquivo_de_gramática* referencia um arquivo **.grmr** válido e *arquivo_fonte*
referencia um arquivo **.prog** escrito na linguagem descrita pelo arquivo de gramática.

## Autores

* **Ygor de Paula** - *Orientando*
* **Alexandre Bittencourt** - *Orientador*
