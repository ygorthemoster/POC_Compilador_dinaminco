const fs    = require('fs');
const path  = require('path');

RegExp.prototype.toJSON = function () {
  return `/${this.source}/`;
}

if(process.argv.length != 4){
  console.log(`node ${process.argv[1]} <GRAMMAR> <PROGRAM>`)
  return -1;
}

// grammar file cleanup
let file = fs.readFileSync(path.join( __dirname, process.argv[2]))
              .toString( )
              .split('\n')
              .map((line) => {
                return line.trim( )
              })
              .filter((line) => {
                return line != '';
              })
              .join('\n');

fs.writeFileSync('clean.txt', file);

// lexical parser of grammar file
let tokens = file.split('')
              .reduce((tokenizer, chr) => {
                  do {
                    consume = true;
                    switch (tokenizer.state) {
                      case 'INITIAL':
                        if(chr == '<') {
                          tokenizer.state = 'TOKEN';
                          tokenizer.lexem = '';
                        } else if (chr == '"') {
                          tokenizer.state = 'CONSTANT';
                          tokenizer.lexem = '';
                        } else if (chr == '/') {
                          tokenizer.state = 'REGEX';
                          tokenizer.lexem = '';
                        } else if (chr == ';') {
                          tokenizer.state = 'COMMENT';
                          tokenizer.lexem = '';
                        } else if (chr == ':') {
                          tokenizer.state = 'COLON';
                        } else if (chr == '|') {
                          tokenizer.tokens.push({type: 'OR'})
                        } else if (chr == '(') {
                          tokenizer.tokens.push({type: 'NODE_BEGIN'})
                        } else if (chr == ')') {
                          tokenizer.tokens.push({type: 'NODE_END'})
                        } else if (chr == ',') {
                          tokenizer.tokens.push({type: 'COMMA'})
                        } else if (chr >= '0' && chr <= '9') {
                          tokenizer.state = 'NUMBER';
                          tokenizer.lexem = chr;
                        } else if (chr >= 'a' && chr <= 'z') {
                          tokenizer.state = 'ID';
                          tokenizer.lexem = chr;
                        } else if (chr >= 'A' && chr <= 'Z') {
                          tokenizer.state = 'SPECIAL';
                          tokenizer.lexem = chr;
                        } else if (chr != ' ' && chr != '\t' && chr != '\n' && chr != '\r') {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        }
                        break;
                      case 'TOKEN':
                        if ((chr >= 'a' && chr <= 'z') ||
                            (chr >= 'A' && chr <= 'Z') ||
                            (chr >= '0' && chr <= '9') ||
                            (chr == '_')) {
                          tokenizer.lexem += chr;
                        } else if (chr == '>'){
                          tokenizer.tokens.push({type: 'TOKEN', id: tokenizer.lexem});
                          tokenizer.state = 'INITIAL';
                        } else {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        }
                        break;
                      case 'CONSTANT':
                        if (chr == '\t' || chr == '\r' || chr == '\n') {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        } else if (chr == '"'){
                          tokenizer.tokens.push({type: 'CONSTANT', id: tokenizer.lexem});
                          tokenizer.state = 'INITIAL';
                        } else {
                          tokenizer.lexem += chr;
                        }
                        break;
                      case 'REGEX':
                        if (chr == '\t' || chr == '\r' || chr == '\n') {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        } else if (chr == '\\'){
                          tokenizer.lexem += chr;
                          tokenizer.state = 'REGEX_IGNORE';
                        } else if (chr == '/'){
                          tokenizer.tokens.push({type: 'REGEX', expr: new RegExp(tokenizer.lexem)});
                          tokenizer.state = 'INITIAL';
                        } else {
                          tokenizer.lexem += chr;
                        }
                        break;
                      case 'REGEX_IGNORE':
                        if (chr == '\t' || chr == '\r' || chr == '\n') {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        } else {
                          tokenizer.lexem += chr;
                          tokenizer.state = 'REGEX';
                        }
                        break;
                      case 'COMMENT':
                        if (chr == '\n') {
                          tokenizer.state = 'INITIAL';
                        }
                        break;
                      case 'COLON':
                        if (chr == ':') {
                          tokenizer.state = 'ATTRIB';
                        } else {
                          consume = false;
                          tokenizer.tokens.push({type: 'COLON'});
                          tokenizer.state = 'INITIAL';
                        }
                        break;
                      case 'ATTRIB':
                        if (chr == '=') {
                          tokenizer.state = 'INITIAL';
                          tokenizer.tokens.push({type: 'ATTRIB'});
                        } else {
                          console.error(`ERRO léxico (caractere ${chr} inesperado no estado ${tokenizer.state}) em ${tokenizer.line}:${tokenizer.column}`);
                          process.exit(1);
                        }
                        break;
                      case 'NUMBER':
                        if (chr >= '0' && chr <= '9') {
                          tokenizer.lexem += chr;
                        } else {
                          consume = false;
                          tokenizer.tokens.push({type: 'NUMBER', value: parseInt(tokenizer.lexem)});
                          tokenizer.state = 'INITIAL';
                        }
                        break;
                      case 'ID':
                        if ((chr >= 'a' && chr <= 'z') ||
                            (chr == '_' )) {
                          tokenizer.lexem += chr;
                        } else {
                          consume = false;
                          tokenizer.tokens.push({type: 'ID', value: tokenizer.lexem});
                          tokenizer.state = 'INITIAL';
                        }
                        break;
                      case 'SPECIAL':
                        if ((chr >= 'A' && chr <= 'Z') ||
                            (chr == '_' )) {
                          tokenizer.lexem += chr;
                        } else {
                          consume = false;
                          switch (tokenizer.lexem) {
                            case 'IGNORE':
                              tokenizer.tokens.push({type: 'IGNORING'});
                              break;
                            case 'ROOT':
                              tokenizer.tokens.push({type: 'ID', value: 'ROOT'});
                              break;
                            case 'NIL':
                              tokenizer.tokens.push({type: 'NULL'});
                              break;
                            case 'PASS':
                              tokenizer.tokens.push({type: 'ID', value: 'ghost'});
                              break;
                            default:
                              console.error(`ERRO léxico (token_especial ${tokenizer.lexem} inesperado) em ${tokenizer.line}:${tokenizer.column}`);
                              process.exit(1);
                          }
                          tokenizer.state = 'INITIAL';
                        }
                        break;
                      default:
                        console.error(`ERRO léxico (estado ${tokenizer.state} inesperado) em ${tokenizer.line}:${tokenizer.column}`);
                        process.exit(1);
                    }
                  } while(!consume);

                  if(chr == '\n') {
                    tokenizer.line++;
                    tokenizer.column = 1;
                  } else {
                    tokenizer.column++;
                  }

                  return tokenizer;
                }, {
                  state: 'INITIAL',
                  lexem: '',
                  line: 1,
                  column: 1,
                  tokens: []
                }).tokens;
                tokens.push({type: 'EOF'});

fs.writeFileSync('tokens.json', JSON.stringify(tokens, null, 4));

// sintatic parser of grammar file
let rules = tokens.reduce((parser, token) => {
                      do {
                        consume = true;
                        switch (parser.state) {
                          case 'INITIAL':
                            if(token.type == 'ID') {
                              parser.state = 'RULE';
                              parser.rule = token.value;
                            } else if(token.type == 'IGNORING') {
                              parser.state = 'IGNORE';
                            } else if(token.type != 'EOF') {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'RULE':
                            if(token.type == 'ATTRIB') {
                              parser.state = 'CHAIN';
                              parser.chain = [];
                              parser.node = '';
                              parser.nullable = false;
                              if(!parser.grammar.rules[parser.rule]){
                                parser.grammar.rules[parser.rule] = [];
                              }
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'CHAIN':
                            if(token.type == 'TOKEN') {
                              parser.chain.push({type:'NON_TERMINAL', id: token.id});
                            } else if(token.type == 'CONSTANT') {
                              parser.chain.push({type:'TERMINAL', expr: new RegExp('^' + token.id.replace(/[.*+?^${}()|[\]\\]/, '\\$&'))});
                            } else if(token.type == 'REGEX') {
                              parser.chain.push({type:'TERMINAL', expr: new RegExp('^' + token.expr.source)});
                            } else if(token.type == 'NULL') {
                              parser.chain.push({type:'TERMINAL', expr: /()/});
                              parser.nullable = true;
                            } else if(token.type == 'NODE_BEGIN') {
                              parser.state = 'NODE';
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'NODE':
                            if(parser.nullable && parser.chain.length != 1){
                              console.error(`ERRO sintático (NIL precisa estar sozinho) em ${parser.token}`);
                              process.exit(1);
                            }

                            if(token.type == 'ID') {
                              parser.node = token.value;
                              parser.args = [];
                              parser.state = 'NODE_ARGUMENTS';
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                          break;
                          case 'NODE_ARGUMENTS':
                            if(token.type == 'COLON') {
                              parser.state = 'NODE_HAS_ARGUMENTS';
                            } else if(token.type == 'NODE_END') {
                              parser.state = 'RULE_END';
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'NODE_HAS_ARGUMENTS':
                            if(token.type == 'NUMBER') {
                              parser.args.push(token.value);
                              parser.state = 'NODE_MORE_ARGUMENTS';
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'NODE_MORE_ARGUMENTS':
                            if(token.type == 'COMMA') {
                              parser.state = 'NODE_HAS_ARGUMENTS';
                            } else if(token.type == 'NODE_END') {
                              parser.state = 'RULE_END';
                            } else {
                              console.error(`ERRO sintático (token ${token.type} inesperado) em ${parser.token}`);
                              process.exit(1);
                            }
                            break;
                          case 'RULE_END':
                            parser.grammar.rules[parser.rule].push({chain: [...parser.chain], node: parser.node, arguments: [...parser.args]});

                            if(token.type == 'OR') {
                              parser.state = 'CHAIN';
                              parser.chain = [];
                              parser.node = '';
                              parser.nullable = false;
                            } else {
                              consume = false;
                              parser.state = 'INITIAL';
                              parser.chain = [];
                              parser.node = '';
                              parser.nullable = false;
                              parser.rule = null;
                            }
                            break;
                          case 'IGNORE':
                            if(token.type == 'CONSTANT') {
                              let special = token.id;

                              special = special.replace(/\\0/g, '\0');
                              special = special.replace(/\\'/g, '\'');
                              special = special.replace(/\\"/g, '\"');
                              special = special.replace(/\\\\/g, '\\');
                              special = special.replace(/\\n/g, '\n');
                              special = special.replace(/\\r/g, '\r');
                              special = special.replace(/\\v/g, '\v');
                              special = special.replace(/\\t/g, '\t');
                              special = special.replace(/\\b/g, '\b');
                              special = special.replace(/\\f/g, '\f');

                              parser.chain.push(special);
                            } else {
                              parser.grammar.breaks = [...parser.grammar.breaks, ...parser.chain];
                              consume = false;
                              parser.state = 'INITIAL';
                              parser.chain = [];
                            }
                            break;
                          default:
                            console.error(`ERRO sintático (estado ${parser.state} inesperado) em ${parser.token}`);
                            process.exit(1);
                        }
                      }  while(!consume);

                      parser.token++;
                      return parser;
                    }, {
                      state: 'INITIAL',
                      chain: [],
                      node: '',
                      args: [],
                      nullable: false,
                      rule: null,
                      token: 1,
                      grammar: {
                        breaks: [],
                        rules: {}
                      }
                    }).grammar;

fs.writeFileSync('grammar.json', JSON.stringify(rules, null, 4));

// AST creation of source file

var place = 0;
var prog = fs.readFileSync(path.join( __dirname, process.argv[3] )).toString();
var tries = {};

function nonTerminal(nt) {
  let failbkp = place;
  if(tries[failbkp] && tries[failbkp][nt]) {
    place += tries[failbkp][nt].consumed;
    return tries[failbkp][nt].processed;
  }

  for (let i = 0; i < rules.rules[nt].length; i++) {
    let fail = false;
    let processed = [];

    place = failbkp;
    for (let j = 0; j < rules.rules[nt][i].chain.length; j++) {
      let tmp = null;

      if(rules.rules[nt][i].chain[j].type == 'NON_TERMINAL') {
        tmp = nonTerminal(rules.rules[nt][i].chain[j].id);
      } else if(rules.rules[nt][i].chain[j].type == 'TERMINAL') {
        tmp = terminal(rules.rules[nt][i].chain[j].expr);
      } else {
        console.error(`ERRO semântico (tipo inesperado: ${rules.rules[nt][i].chain[j].type}) ao processar: nonTerminal(${nt})`);
        process.exit(1);
      }

      if(tmp != null) {
        processed.push(tmp);
      } else {
        fail = true;
        break;
      }
    }

    if(!fail) {
      let node = require(`./nodes/${rules.rules[nt][i].node}`);

      if(!tries[failbkp])
        tries[failbkp] = {};

      tries[failbkp][nt] = {
        consumed: place - failbkp,
        processed: new node(processed.filter((node, idx) => rules.rules[nt][i].arguments.includes(idx)))
      }

      return tries[failbkp][nt].processed;
    }
  }

  return null;
}

function terminal(t) {
  while (rules.breaks.includes(prog[place]))
    place++;

  let found = t.exec(prog.substring(place));
  if(found) {
    place += found[0].length;
    return found[0];
  }

  return null;
}

let tree = nonTerminal('ROOT');
fs.writeFileSync('ast.json', JSON.stringify(tree, null, 4));

// AST interpretation of source file
while (rules.breaks.includes(prog[place])) place++;
if(place != prog.length){
  console.error(`ERRO semântico (arquivo não completamente processado)`);
  process.exit(1);
}

tree.interpret();
