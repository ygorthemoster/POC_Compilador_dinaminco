const Generic = require('./generic.js');

class PrintNode extends Generic {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    return {
      root: key == '',
      type: 'Print',
      expr: this.children[0]
    }
  }

  interpret() {
    console.log(this.children[0].interpret());
  }
}

module.exports = PrintNode;
