const BinOp = require('./bin_op.js');

class SubstractionNode extends BinOp {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type += ': Subtraction';
    return json
  }

  operation(l, r) {
    return l - r;
  }
}

module.exports = SubstractionNode;
