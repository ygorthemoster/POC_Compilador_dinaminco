const Generic = require('./generic.js');

class FloatNode extends Generic {
  constructor() {
    super(...arguments);
    this.val = parseFloat(`${this.children[0].val}.${this.children[1].val}`);
  }

  toJSON(key) {
    return {
      root: key == '',
      type: 'Float',
      value: this.val
    }
  }

  interpret() {
    return this.val;
  }
}

module.exports = FloatNode;
