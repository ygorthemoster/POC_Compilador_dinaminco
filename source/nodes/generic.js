var id = 0;

var symbolTable = {};

class GenericNode {
  constructor() {
    this.id = ++id;
    this.children = [...arguments];

    this.children = this.children[0];
  }

  toJSON(key) {
    return {
      root: key == '',
      type: 'Generic',
      children: this.children
    };
  }

  interpret() {
    console.log(JSON.stringify(this));
  }

  getTableSymbol(symbol){
    return symbolTable[symbol];
  }

  setTableSymbol(symbol, value){
    symbolTable[symbol] = value;
  }
}

module.exports = GenericNode;
