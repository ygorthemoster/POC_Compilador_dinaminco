const BinOp = require('./bin_op.js');

class AdditionNode extends BinOp {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type += ': Addition';
    return json
  }

  operation(l, r) {
    return l + r;
  }
}

module.exports = AdditionNode;
