const List = require('./list.js');

class IDListNode extends List {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type = 'Identifiers List'
    return json
  }

  interpret() {
    return this.children;
  }
}

module.exports = IDListNode;
