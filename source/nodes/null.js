class NULLNode {
  constructor() {}
  interpret() {}
  toJSON(key) {
    return {
      root: key == '',
      type: 'NULL'
    };
  }
}

module.exports = NULLNode;
