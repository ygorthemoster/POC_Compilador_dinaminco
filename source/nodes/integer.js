const Generic = require('./generic.js');

class IntegerNode extends Generic {
  constructor() {
    super(...arguments);
    this.val = '';
    for (var i = 0; i < this.children.length; i++) {
      if(this.children[i] instanceof IntegerNode){
        this.val += this.children[i].val;
      } else {
        this.val += this.children[i];
      }
    }


    this.val = parseInt(this.val);
  }

  toJSON(key) {
    return {
      root: key == '',
      type: 'Int',
      value: this.val
    }
  }

  interpret() {
    return this.val;
  }
}

module.exports = IntegerNode;
