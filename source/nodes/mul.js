const BinOp = require('./bin_op.js');

class MultiplicationNode extends BinOp {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type += ': multiplication';
    return json
  }

  operation(l, r) {
    return l * r;
  }
}

module.exports = MultiplicationNode;
