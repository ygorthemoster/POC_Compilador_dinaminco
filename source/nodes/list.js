const Generic = require('./generic.js');

class ListNode extends Generic {
  constructor() {
    super(...arguments);

    let tmp = [];

    for (let i = 0; i < this.children.length; i++) {
      if(this.children[i] instanceof ListNode){
        tmp = tmp.concat(this.children[i].children);
      } else {
        tmp.push(this.children[i]);
      }
    }

    this.children = tmp;
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type = 'List'
    return json
  }

  interpret() {
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].interpret();
    }
  }
}

module.exports = ListNode;
