const Generic = require('./generic.js');

class DeclarationNode extends Generic {
  constructor() {
    super(...arguments);
    this.type = this.children[0];
    this.ids = [];

    for (let i = 1; i < this.children.length; i++) {
      if(this.children[i] instanceof IDListNode){
        this.ids = this.ids.concat(this.children[i].children);
      } else {
        this.ids.push(this.children[i]);
      }
    }

  }

  toJSON(key) {
    
  }

  interpret() {

  }
}

module.exports = ListNode;
