const BinOp = require('./bin_op.js');

class PowerNode extends BinOp {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type += ': exponentiation';
    return json
  }

  operation(l, r) {
    return Math.pow(l, r);
  }
}

module.exports = PowerNode;
