const BinOp = require('./bin_op.js');

class ModuloNode extends BinOp {
  constructor() {
    super(...arguments);
  }

  toJSON(key) {
    let json = super.toJSON();

    json.type += ': modulo';
    return json
  }

  operation(l, r) {
    return l % r;
  }
}

module.exports = ModuloNode;
