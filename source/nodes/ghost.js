class GhostNode {
  constructor(node) {
    while (Array.isArray(node))
      node = node[0];
      
    return node;
  }
}

module.exports = GhostNode;
