const Generic = require('./generic.js');

class BinOpNode extends Generic {
  constructor() {
    super(...arguments);

    this.l = this.children[0];
    this.r = this.children[1];
  }

  toJSON(key) {
    return {
      root: key == '',
      type: 'binOp',
      left: this.l,
      right: this.r
    }
  }

  interpret() {
    return this.operation(this.l.interpret(), this.r.interpret());
  }

  operation(l, r) {
    return console.log(l, r);
  }
}

module.exports = BinOpNode;
