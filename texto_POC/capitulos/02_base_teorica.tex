% ----------------------------------------------------------
% Refenrencial teorico
% ----------------------------------------------------------
\chapter[Fundamentação Teórica]{Fundamentação Teórica}

Este trabalho utiliza conceitos das áreas de Teoria de Linguagens e Compiladores.
Esta seção apresenta os principais conceitos utilizados no trabalho.

\section{Notação BNF} \label{sec:notação_bnf}

A descrição de uma linguagem de programação deve ser feita de maneira clara e precisa.
Para este fim, em 1958, Jhon Backus propos uma \textit{metalinguagem} de fórmulas
metalinguísticas para descrever o ALGOL, sua nova linguagem de programação \cite{algol60}.
Desde então, a forma de Backus-Naur (BNF) vem sendo a forma majoritaria que é usada
para descrever linguagens de programação.

A BNF é composta por diversas regras escritas na forma: \textbf{produção ::= expressão}
onde produção representa um não-terminal da gramática e expressão é composta por terminais
e não terminais. Cada uma dessas regras indica que a expressão à direita é derivada
a partir do não terminal à esquerda.

A expressão de uma BNF usa nomes entre \(\langle\) e \(\rangle\) para identificar
um não terminal e cadeias de caracteres entre aspas duplas para representar cadeias
literais de terminais. A notação BNF também usa o caracter | para indicar expressões
alternativas. Podemos observar essas regras, por exemplo, na BNF que define operações
matemáticas incluindo as quatro operações basicas (+, -, *, /) e o uso de parênteses:

\begin{samepage}
  \[ express\tilde{a}o ::= \langle parcela \rangle ``+" \langle express\tilde{a}o \rangle \:|\: \langle parcela \rangle ``-" \langle express\tilde{a}o \rangle \:|\: \langle parcela \rangle \]
  \[ parcela ::= \langle fator \rangle ``*" \langle parcela \rangle \:|\: \langle fator \rangle ``/" \langle parcela \rangle \:|\: \langle fator \rangle \]
  \[ fator ::= ``(" \langle express\tilde{a}o \rangle ``)" \:|\: \langle n\acute{u}mero \rangle \]
  \[ n\acute{u}mero ::= \langle decimal \rangle \:|\: \langle inteiro \rangle \]
  \[ decimal ::= \langle inteiro \rangle ``." \langle inteiro \rangle \]
  \[ inteiro ::= \langle digito \rangle \ \langle inteiro \rangle \]
  \[ digito ::= ``0" \:|\: ``1" \:|\: ``2" \:|\: ``3" \:|\: ``4" \:|\: ``5" \:|\: ``6" \:|\: ``7" \:|\: ``8" \:|\: ``9" \]
\end{samepage}

Nessa definição, podemos traduzir, por exemplo, \( fator ::= \allowbreak ``(" \langle express\tilde{a}o \rangle ``)" \allowbreak \:|\: \allowbreak \langle n\acute{u}mero \rangle \)
como: um \textbf{fator} é composto de uma \textbf{expressão} entre parênteses ou
por um \textbf{número}, e, um número por sua vez é descrito na regra \( n\acute{u}mero ::= \langle decimal \rangle \:|\: \langle inteiro \rangle \).

\section{Expressões regulares}

Expressões regulares (Regex ou Regular Expression em ingles), são um formalismo
matemático usado em estudos de teoria de linguagens.
Expressões regulares são normalmente empregadas na busca de sub-cadeias de caracteres em um texto.
O conceito surgiu nos anos 50 quando Kleene formalizou matematicamente a descrição de uma linguagem regular \cite{automata}.

Expressões regulares são usadas em diversas aplicações de busca que definem diferentes
sintaxes e padrões que apesar de similares possuem diferenças sutis.
Para este trabalho, usaremos a sintaxe da linguagem de programação \textit{JavaScript} \cite{MDNregex}.

Uma expressão regular é composta por uma combinação de caracteres simples e caracteres
especiais. Os caracteres especiais são usados para denotar repetições, classes de
caractere ou sub-expressões. Podemos observar alguns desses caracteres ao observar
um Regex simplificado que identifica a maioria dos e-mails válidos.

\[ [0-9a-zA-Z][0-9a-zA-Z\\.]\left\{0, 63\right\}@[a-zA-Z][0-9a-zA-Z]*\\.[a-zA-Z]+ \]

Neste exemplo podemos ver o uso de um caracter simples (@) indicando que este caracter
deve aparecer exatamente. Além disso temos classes de caracter, delimitados por colchetes,
indicando que deverão ser aceitos qualquer caractere na classe. Observamos também \textit{ranges},
por exemplo \textbf{a-z} que indica a aceitação de qualquer caractere entre \textbf{a}
e \textbf{z}. E, finalmente indicadores de repetição (+, * e \(\left\{n, m \right\}\))
que indicam que o caracter ou classe de caracteres anterior deve ser lido uma ou
mais vezes, zero ou mais vezes e entre \textit{n} e \textit{m} vezes, respectivamente.

\section{Compiladores}

Um compilador é um software que traduz um programa escrito em uma linguagem, chamada
de \textit{fonte}, à outra linguagem, conhecida como \textit{destino}. Para completar
esta tarefa, o compilador é dividido em duas etapas \cite{livro_dragao_segundaedicao} \cite{compiladores_modernos}.

Primeiramente, o compilador executa a etapa de análise onde realiza três tipos de
análise do código-fonte: análise léxica, análise sintática e análise semântica.
Ao fim da etapa de análise, é gerada uma representação intermediária do arquivo fonte que
pode então ser passada ao próximo passo. O segundo passo é conhecido como síntese
e é responsável por gerar o código na linguagem de destino, e, opcionalmente, realizar
diversos tipos de otimização.

Este trabalho consiste na criação de um compilador, porém, foca principalmente no
estágio de análise, portanto é preciso compreender os conceitos básicos que são
usados durante os três passos da análise: Análise Léxica, Análise Sintática e Análise
Semântica.

\subsection{Análise Léxica}

A primeira das análises de um compilador, a análise léxica, tem como objetivo traduzir
as cadeias de caracteres que compõe o código-fonte para uma sequência de \textit{tokens},
onde cada token representa uma unidade com significado, por exemplo, identificadores
ou operadores. Essa análise é feita para que os próximos passos do compilador possam
ignorar o processamento de cadeias de caracteres, assim simplificando o processo.

O passo de análise léxica é estudado e aplicado em diversas áreas principalmente no
processamento de linguagem natural, onde é usado para tokenizar frases. Por este
motivo, é um processo com algoritmos conhecidos e eficientes. Normalmente, em um
compilador, essa análise é implementada usando um autômato finito ou expressões
regulares que representam as definições léxicas da linguagem.

\subsection{Análise Sintática}

Após a análise léxica, o compilador realiza a análise sintática, com objetivo de
validar as regras de construção da linguagem. Regras descritas utilizando alguma
notação formal para descrição de gramáticas, por exemplo, a notação BNF.

O analisador recebe a cadeia de \textit{tokens} gerada anteriormente e verifica
se a cadeia pode ser gerada pela gramática da linguagem fonte. Nesta etapa, é esperado
que sejam apontados erros de sintaxe, ou seja, erros na estrutura ou gramática utilizadas
no código-fonte. Para isso, é gerada uma árvore de derivação, que representa os
passos ou a sequência de derivações necessárias para se chegar à cadeia de tokens específica da entrada,
a partir da produção base da gramática que é a produção do símbolo inicial da gramática.
Na Figura \ref{fig:árvore_derivação}, é dado um exemplo de árvore de derivação
da cadeia \((1/2) - 3.3 + 40 * 5\) utilizando a gramática apresentada na Seção \ref{sec:notação_bnf}.

\begin{figure}[ht]
  \caption{Árvore de derivação da expressão \( (1 / 2) - 3.3 + 40 * 5 \)}
  \includegraphics[width=0.8\textwidth]{arvore_derivacao}
  \centering
  \label{fig:árvore_derivação}
\end{figure}

A árvore de derivação pode ser gerada utilizando métodos descendentes ou ascendentes.
Métodos ascendentes geram a árvore de derivação aglutinando os terminais
à medida que estes aparecem na cadeia de tokens usando uma pilha ou algum outro
método similar. O método escolhido nesse trabalho é um método descendente, que
começa a análise sintática pelo símbolo inicial da gramática e vai aplicando uma
sequência de regras da gramática para tentar derivar a cadeia de entrada. Existem
dois métodos principais de análise descendente: o método baseado em tabela e o método
de descida recursiva \cite{livro_dragao_segundaedicao}.

O método baseado em tabela constrói uma tabela de análise com base nos conjuntos
\textit{First} (conjuntos de iniciadores) e \textit{Follow} (conjuntos de seguidores) da gramática e utiliza uma pilha para controlar o processo
de derivação. Já o método de descida recursiva, utilizado nesse trabalho, define,
para cada não-terminal, uma função que implementa as regras sintáticas do não-terminal.
Por exemplo, para um não-terminal \textbf{fator}, uma chamada à função \textbf{fator()}
tem a finalidade de encontrar a maior cadeia que pode ser derivada do não-terminal
fator. As funções são chamadas recursivamente a partir da função que representa
o símbolo inicial da gramática \cite{livro_dragao_segundaedicao}.

\subsection{Análise Semântica}

A última análise, conhecida como análise semântica, tem como objetivo atribuir significado
às construções e expressões da linguagem. Essa fase é responsável por gerar o código
intermediário que será passado ao passo de síntese. Normalmente é realizada juntamente
com a análise sintática, e ajuda a fazer verificações dependentes de contexto como,
por exemplo, tipagem e \textit{null values}.

Ao final da análise semântica, é gerado um código intermediário, que normalmente está
representado como uma árvore de sintaxe abstrata (\textit{abstract syntax tree}
ou AST em inglês) ou um código de três endereços. Neste trabalho usaremos a representação
em AST.

\begin{figure}[ht]
  \caption{AST da expressão \( (1 / 2) - 3.3 + 40 * 5 \)}
  \includegraphics[width=0.6\textwidth]{arvore_abstrata}
  \centering
  \label{fig:árvore_sixtáxe_abstrata}
\end{figure}

Uma Árvore de sintaxe abstrata é uma representação em árvore das construções da
linguagem, ou seja, é uma representação onde cada comando e expressão de uma linguagem
é um nó da árvore, que, em sua completude, representa todo o código fonte. A vantagem
da AST em relação à árvore de derivação é que a AST representa de forma mais concisa
todas as construções presentes no código fonte de entrada. A AST não representa
os passos da derivação e também não representa caracteres de pontuação e de estruturação
do código. Por exemplo, cada expressão é representada como uma subárvore que tem
o operador da expressão como nó pai e os nós filhos são os operandos do lado esquerdo
e do lado direito. Os operandos do lado esquerdo e do lado direito podem ser outras
expressões como pode ser visto na Figura \ref{fig:árvore_sixtáxe_abstrata}.

\subsection{Interpretação}

Um interpretador de linguagem de programação é um programa que executa um algoritmo
escrito em uma linguagem de programação, sem a geração de um executável próprio \cite{livro_dragao_segundaedicao}.
Um interpretador executa as mesmas análises de um compilador, porém ao invés de
gerar o código na linguagem de destino, ele o executa diretamente.

Normalmente usados por linguagens de script, interpretadores possuem algumas vantagens
em relação à compiladores. A principal delas sendo a portabilidade. Porém, também
possuem desvantagens como, por exemplo, o desempenho que normalmente é pior em relação à versões compiladas
de um mesmo código fonte.

A interpretação da linguagem pode ser realizada a partir de um caminhamento na AST.
Começando pela raiz e avaliando cada nó em ordem, o interpretador consegue executar todas as instruções do programa.
Cada nó da árvore define uma função que implementa a lógica de como interpretar esse nó, ou seja, cada nó
sabe como deve ser interpretado.


\section{Linguagens de domínio específico}

O termo Linguagens de domínio específico (\textit{domain specific languages} ou
DSL em inglês) ganhou popularidade com o aumento da modelagem de domínio especifico
mas seu conceito é muito anterior à isto. DSLs são linguagens de programação que
através de notações e abstrações apropriadas, oferecem um poder expressivo focado,
e normalmente restrito, em um determinado problema ou domínio \cite{dsl_biblio}.

DSLs usualmente são pequenas, oferecem apenas um pequeno conjunto de notações e
abstrações e normalmente são declarativas, ou seja, expressam o algoritmo em função
de quais tarefas ele deve fazer ao invés de como cada tarefa deve ser feita. Exemplos
de DSL incluem, mas não estão limitados a: HTML (voltada para a criação de paginas
web), SQL (uma linguagem para buscas em bancos de dados), PIC (para a geraçao de
imagens) e LaTeX (usado na criação de documentos). \cite{DSLEng}

\subsection{Ambientes para criação de DSLs}

A implementação de uma DSL traz diversos beneficios se aplicada corretamente \cite{mbeddr},
porém implica em um processo custoso tecnicamente para sua implementação. Nesse
contexto foram desenvolvidas ferramentas para facilitar a criação e uso dessas DSLs.
Esses ambientes para criação de DSLs podem tomar diversas formas, de \textit{plug-ins}
à IDEs completas.

Existem diversos ambientes de criação de DSLs. Alguns exemplos de tais ambientes
são, \textit{xText} \cite{xtext}, um \textit{plug-in} para a IDE eclipse criado como
um framework para criação de editores textuais e parte do projeto oAW, \textit{MPS}
\cite{mps}, um ambiente integrado criado pela \textit{JetBrains} para a engenharia
de linguagens, \textit{JGroovy} \cite{JGroovy}, uma implementação extensível
combinando as linguagens Java e Groovy, entre outras como visual studio tools,
\textit{OCPs} e \textit{XPC}.

Alguns destes ambientes se propõe a solucionar problemas parecidos com o desse trabalho
e serão discutidos em mais detalhe no Capítulo \ref{cap:trabalhos_relacionados} de
trabalhos relacionados.
