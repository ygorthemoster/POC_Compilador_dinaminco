% ----------------------------------------------------------
% metodologia
% ----------------------------------------------------------
\chapter[Metodologia]{Metodologia} \label{cap:metodologia}

Este trabalho consiste no desenvolvimento e na implementação de um interpretador capaz de adaptar sua
gramática de acordo com um arquivo de definição. Para realizar essa implementação e verificar sua corretude e completude,
os seguintes passos foram executados neste trabalho:

\begin{enumerate}
  \item Analisar e reduzir a gramática de definição;
  \item Realizar as análises léxica e sintática utilizando a gramática obtida no passo anterior;
  \item Fazer o carregamento dos nós da AST;
  \item Gerar, à partir dos nós e do arquivo-fonte, uma AST;
  \item Realizar a interpretação da AST gerada no passo anterior.
\end{enumerate}

\section{Gramática de descrição de linguagens} \label{sec:grmr}

A gramática de descrição segue um padrão parecido com a notação BNF descrita na
Seção \ref{sec:notação_bnf}. Cada linguagem é descrita por uma sequência de regras,
que indicam uma ou mais produções e, para cada uma, o nó da AST equivalente à essa
produção. Cada regra pode ser definida em alto nível pela notação BNF como:

\[ regra\_gramatical ::= \langle identificador \rangle ``::=" \langle lista\_produ\textrm{\c{c}}\tilde{o}es \rangle \]
\[ lista\_produ\textrm{\c{c}}\tilde{o}es ::= \langle produ\textrm{\c{c}}\tilde{a}o \rangle \langle lista\_produ\textrm{\c{c}}\tilde{o}es \rangle \:|\: \langle produ\textrm{\c{c}}\tilde{a}o \rangle \]
\[ produ\textrm{\c{c}}\tilde{a}o ::= \langle deriva\textrm{\c{c}}\tilde{a}o \rangle \langle n\acute{o}\_equivalente \rangle \]

Os identificadores são compostos de caracteres entre \textbf{A} e \textbf{Z}
minúsculos ou maiúsculos, números e o caractere underline (\textbf{\_}). As cadeias
de produção em si são definidas por uma sequência de terminais e não terminais,
definidos pelas regras:

\[ deriva\textrm{\c{c}}\tilde{a}o ::= \langle termo \rangle \langle deriva\textrm{\c{c}}\tilde{a}o \rangle \:|\: \langle termo \rangle \]
\[ termo ::= \langle n\tilde{a}o\_terminal \rangle \:|\: \langle constante \rangle \:|\: \langle express\tilde{a}o\_regular \rangle \]
\[ n\tilde{a}o\_terminal ::= ``\langle" \langle identificador \rangle ``\rangle" \]
\[ constante ::= ````" \langle identificador \rangle ``"" \]
\[ express\tilde{a}o\_regular ::= ``\/" \langle express\tilde{a}o \rangle ``\/" \]

Onde \textit{expressão} é qualquer expressão regular válida. Os nós de equivalência
são compostos do nome de um nó e a lista de parâmetros deste. A lista de parâmetros
é uma lista de indices que indica quais produções são filhas deste nó. Na
forma BNF, o nó equivalente é descrito como:

\[ n\acute{o}\_equivalente ::= ``(" \langle identificador \rangle ``:" \langle lista\_par\hat{a}metros \rangle ``)" \]
\[ lista\_par\hat{a}metros ::= \langle \acute{i}ndice \rangle \langle lista\_par\hat{a}metros \rangle \:|\: \langle \acute{i}ndice \rangle \]

Para completar a gramática foram definidas quatro palavras chaves \textbf{ROOT},
\textbf{IGNORE}, \textbf{NIL} e \textbf{PASS} elas são usadas para representar,
respectivamente, a produção inicial da gramática, os caracteres que deverão ser
ignorados pelo analisador, a produção vazia e uma produção que deve ser ignorada
na construção da AST. Exemplos de gramáticas podem ser encontradas nos apêndices
\ref{apnd:mat} e \ref{apnd:port}.

\section{Análise e simplificação da gramática}

A gramática é analisada utilizando um processo similar ao de um compilador. São três
passos de análise: léxica, sintática e semântica.
Os três analisadores foram construídos baseados em redutores funcionais, isto é, cada
um aplica uma função em todos elementos de um arranjo em ordem junto com um acumulador
que, ao fim, contém o resultado do passo de análise em questão.

Após a análise, são removidas as produções inalcançáveis, para a redução da complexidade da gramática.
Essa remoção é feita com um simples algoritmo de busca em profundidade na gramática.
Ao fim desse passo, o analisador está pronto para analisar o arquivo-fonte.

\section{Detalhes da estrutura do compilador}

Para simplificar a gramática de descrição do compilador, as três etapas de análise
(léxica, sintática e semântica) são executadas ao mesmo tempo. Para realizar a análise
léxica junto com a análise sintática descendente, o compilador utiliza como símbolos
terminais os lexemas da linguagem.

O método de análise escolhido foi o de descida recursiva. Foram definidas
duas funções genéricas. Uma dessas funções consome qualquer cadeia de terminais,
isto é, caracteres, e retorna se teve sucesso ou não no reconhecimento da cadeia.
A outra função testa iterativamente todas as regras referentes a um não terminal,
fazendo um \textit{backup} do estado atual no inicio de cada tentativa. Esse \textit{backup}
é restaurado no caso da falha desta tentativa. Estas funções pode ser descrita pelos
pseudocódigos das Figuras \ref{fig:cod_term} e \ref{fig:cod_nao_term}.

\begin{figure}[ht]
  \caption{Pesudocodigo: Função terminal}
  \includegraphics[width=0.8\textwidth]{terminal}
  \centering
  \label{fig:cod_term}
\end{figure}

\begin{figure}[ht]
  \caption{Pesudocodigo: Função não terminal}
  \includegraphics[width=0.9\textwidth]{nao_terminal}
  \centering
  \label{fig:cod_nao_term}
\end{figure}

A função dos não terminais retorna um nó da árvore de sintaxe abstrata referente
àquela regra de derivação, ou, retorna uma falha caso não exista regra que possa
ser aplicada. Para aumentar a performance do analisador, foi implementado um
mecanismo de memória que permite ao analisador gravar partes da análise que
já tenham sido completadas, esta memória é acessada quando a função é chamada novamente
na mesma posição para o mesmo terminal ou não-terminal.

\section{Geração da AST}

Além da análise sintática, o analisador sintático tem uma outra tarefa muito importante:
a geração da representação intermediária (RI). Em geral, a RI é gerada de forma
conjunta pelos analisadores sintático e semântico. O analisador semântico através
de regras semânticas é o responsável por preencher diversas informações importantes
presentes na RI, além de fazer verificações no código. Essas informações são
utilizadas, por exemplo, pelas fases de otimização e geração de código de um compilador.

As representações intermediárias mais conhecidas e utilizadas são: árvores de
sintaxe abstrata (AST), grafos acíclicos dirigidos (GAD) e o código de três
endereços. Neste trabalho, será gerada somente a árvore de sintaxe abstrata.

Todo processo de derivação gera uma árvore de derivação também chamada de árvore
de sintaxe concreta. Trabalhar diretamente com a árvore de derivação, em geral,
não é conveniente, uma vez que ela inclui muitos detalhes desnecessários que foram
incluídos na sintaxe da linguagem fonte para resolver problemas de ambiguidade,
precedência, pontuação, clareza, legibilidade, entre outros. Por essa razão, em
alguns casos é conveniente a definição de uma segunda sintaxe chamada sintaxe abstrata,
que ignora todos os aspectos não essenciais da sintaxe original (da sintaxe concreta).
Para representar a sintaxe abstrata, definimos uma árvore de sintaxe abstrata que
é mais concisa do que uma árvore de derivação\cite{livro_dragao_segundaedicao}.

\begin{figure}[ht]
  \caption{Hierarquia dos nós da AST}
  \includegraphics[width=\textwidth]{nos_ast}
  \centering
  \label{fig:diagrama_classes}
\end{figure}

A árvore de sintaxe abstrata é composta de nós que são carregados dinamicamente
durante a execução do analisador. Cada nó estende uma classe de nó genérico que
possui métodos e atributos necessários para a interpretação da árvore como um todo.
A hierarquia de nós da AST é mostrada na Figura \ref{fig:diagrama_classes}

Cada nó deve implementar um construtor, que recebe os filhos deste nó e realiza
a operação necessária que o nó representa. Todos os nós também devem definir uma
função que realizará a interpretação daquele nó. Por exemplo, um nó condicional,
representando um comando \textit{if} deve testar a sua expressão condicional para
saber se chamará a função de interpretação do filho correspondente à parte verdadeira
ou do filho correspondente à parte falsa caso exista.
